package br.com.itau;

import br.com.itau.enums.Status;

import java.util.Scanner;

public class Jogo {

    private int rodada;
    private Status status;

    public int getRodada() {
        return rodada;
    }

    public void setRodada(int rodada) {
        this.rodada = rodada;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Jogo() {
        this.rodada = 0;
        this.status = Status.INICIADO;
        IO.imprimirInicio();
    }

    public void Iniciar(Jogador jogador) {

        while (this.getStatus() != Status.SAIR) {

            this.novaRodada();

            IO.imprimirMensagem(this.getStatus());

            Baralho.criarBaralho();
            Baralho.embaralharBaralho();

            jogador.zerarPontos();

            this.setStatus(Status.INICIADO);

            while (!this.getStatus().equals(Status.PAUSA)) {

                this.novoTurno(jogador);
                this.mudaStatus();

                if (!this.getStatus().equals(Status.PAUSA)) {
                    Carta carta = Baralho.pegarCarta();

                    IO.imprimirCarta(carta);

                    registrarPontos(jogador, carta);

                    IO.imprimirRodada(this);
                    IO.imprimirPontos(jogador);
                    IO.imprimirHistorico(jogador);
                }

            }
        }
    }

    public void registrarPontos(Jogador jogador, Carta carta) {
        jogador.setPontos(carta.getPonto());
        jogador.setHistorico(new Historico(jogador.getPontos(), this.getRodada()));
    }

    public void mudaStatus() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("\n(1)NOVA CARTA (2) PAUSAR");

        Status st = this.status;
        String opcao = scanner.nextLine();

        switch (opcao) {
            case "1":
                st = Status.CONTINUA;
                break;
            case "2":
                st = Status.PAUSA;
                break;
        }

        this.status = st;
    }

    public void novoTurno(Jogador jogador) {
        if (jogador.getPontos() > 21) {
            System.out.printf("Rodada finalizada! Voce estourou 21 pontos.");
            jogador.zerarPontos();
            this.status = Status.PAUSA;
        }
    }

    public void novaRodada() {
        this.rodada++;
    }
}
