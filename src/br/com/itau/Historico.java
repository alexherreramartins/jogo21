package br.com.itau;

public class Historico {
    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getRodada() {
        return rodada;
    }

    public void setRodada(int rodada) {
        this.rodada = rodada;
    }

    private int valor;
    private int rodada;

    public Historico(int valor, int rodada) {
        this.valor = valor;
        this.rodada = rodada;
    }
}
