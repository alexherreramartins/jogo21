package br.com.itau;

import br.com.itau.enums.Naipe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class Baralho {

    public static List<Carta> getCartas() {
        return cartas;
    }

    public static void setCartas(List<Carta> cartas) {
        Baralho.cartas = cartas;
    }

    private static List<Carta> cartas = new ArrayList<>();

    public Baralho(){}

    public static void criarBaralho(){

        //cria atribuições das cartas
        String[] valores = {"A","2","3","4","5","6","7","8","9","10", "J", "Q", "K"};

        int i=1;
        //Criando cartas de copas até o 10
        for (String c : valores) {
            cartas.add(new Carta(c, i++, Naipe.COPAS));
        }

        i=1;
        //criando cartas de paus
        for (String c : valores) {
            cartas.add(new Carta(c, i++, Naipe.PAUS));
        }

        i=1;
        //criando cartas de ouros
        for (String c : valores) {
            cartas.add(new Carta(c, i++,  Naipe.OUROS));
        }

        i=1;
        //criando cartas de espadas
        for (String c : valores) {
            cartas.add(new Carta(c, i++,  Naipe.ESPADAS));
        }
    }

    public static void embaralharBaralho(){
        Collections.shuffle(cartas);
    }

    public static Carta pegarCarta(){

        if (cartas.isEmpty()) {
            throw new IllegalStateException("Baralho vazio!");
        }

        int posicao = new Random().nextInt(cartas.size());

        Carta sorteada = cartas.remove(posicao);

        return sorteada;
    }
}
