package br.com.itau;

import br.com.itau.enums.Status;

public class Main {

    public static void main(String[] args) {
        Jogo jogo = new Jogo();

        Jogador jogador = new Jogador("Jogador1", 0, new Historico(0,0));

        jogo.Iniciar(jogador);

    }
}
