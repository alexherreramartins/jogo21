package br.com.itau.enums;

public enum Naipe {
    OUROS, COPAS, ESPADAS, PAUS
}
