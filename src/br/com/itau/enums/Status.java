package br.com.itau.enums;

public enum  Status{
    INICIADO, CONTINUA, PAUSA, FINALIZADO, SAIR;
}
