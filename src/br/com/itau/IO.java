package br.com.itau;

import br.com.itau.enums.Status;

import java.util.Scanner;

public class IO {

    public static void imprimirInicio() {
        System.out.println("Bem vindo ao jogo 21!");
        System.out.println("Iniciando jogo...");
    }


    public static void imprimirCarta(Carta carta) {

        System.out.println("Carta: " + carta.getValor() + "-" + carta.getNaipe());

    }

    public static void imprimirPontos(Jogador jogador) {

        System.out.println("Pontos: " + jogador.getPontos());

    }

    public static void imprimirHistorico(Jogador jogador) {
        System.out.println("Melhor pontuacao obtida: " + jogador.getHistorico().getValor() + " Rodada: " + jogador.getHistorico().getRodada());
    }

    public static void imprimirRodada(Jogo jogo) {
        System.out.println("Rodada: " + jogo.getRodada());
    }

    public static void imprimirMensagem(Status status) {
       switch (status)
       {
           case PAUSA:
               System.out.println("Voce encerrou esta rodada.");
               break;
           case INICIADO:
               System.out.println("Voce iniciou uma rodada.");
               break;
        }

}
}
