package br.com.itau;

import br.com.itau.enums.Naipe;

public class Carta {
    private String valor;
    private int ponto;
    private Naipe naipe;


    public int getPonto() {
        return ponto;
    }

    public void setPonto(int ponto) {
        this.ponto = ponto;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Naipe getNaipe() {
        return naipe;
    }

    public void setNaipe(Naipe naipe) {
        this.naipe = naipe;
    }

    public Carta(String valor, int ponto, Naipe naipe){
        this.valor = valor;

        if (ponto >= 10){
            this.ponto = 10;
        }else{
            this.ponto = ponto;
        }

        this.naipe = naipe;
    }
}
