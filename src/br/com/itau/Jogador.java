package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Jogador {

    private String nome;
    private int pontos;
    private Historico historico;

    public Jogador(String nome, int pontos, Historico historico) {
        this.nome = nome;
        this.pontos = pontos;
        this.historico = historico;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos += pontos;
    }

    public void zerarPontos(){
        this.pontos = 0;
    }
    public Historico getHistorico() {
        return historico;
    }

    public void setHistorico(Historico historico) {

        if (historico.getValor() > this.historico.getValor() && historico.getValor() <=21) {
            this.historico = historico;
        }
    }

}
